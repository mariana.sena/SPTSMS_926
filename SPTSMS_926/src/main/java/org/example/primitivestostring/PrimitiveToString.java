package org.example.primitivestostring;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class PrimitiveToString {

        public static void main(final String[] args) {
                try {
                        // Reading input from console using BufferedInputStream
                        BufferedInputStream br = new BufferedInputStream(System.in);
                        System.out.print("Enter a string: ");
                        byte[] inputBytes = new byte[100]; // Define a buffer of 100 bytes to read the input
                        int bytesRead = br.read(inputBytes);

                        // Convert the input bytes to a string and remove unwanted whitespace
                        String inputString = new String(inputBytes, 0, bytesRead).trim();

                        // Convert the string to an array of bytes using ASCII
                        byte[] asciiBytes = inputString.getBytes(StandardCharsets.US_ASCII);

                        //Convert the array of bytes to an integer
                        int result = 0;
                        for (byte b : asciiBytes) {
                                result += b;
                        }

                        System.out.println("Input string: " + inputString);
                        System.out.println("Byte array (ASCII): " + java.util.Arrays.toString(asciiBytes));
                        System.out.println("Resulting integer (sum of ASCII values): " + result);

                } catch (IOException e) {
                        System.out.println("An error occurred while reading console input: " + e.getMessage());
                }
        }
}
